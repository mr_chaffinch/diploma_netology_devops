#!/bin/bash

set -e 
KUBESPRAY="./kubespray_ansible"
PWND=$(pwd)

echo -e "\n##################################################################"
echo -e "# Preparing cloud infra "
echo -e "####################################################################\n"
cd terraform
terraform init -backend-config "access_key=$YC_STORAGE_ACCESS_KEY" -backend-config "secret_key=$YC_STORAGE_SECRET_KEY"
terraform workspace select prod
WS=$(terraform workspace show)
terraform apply -auto-approve

echo -e "\n##################################################################"
echo -e "# Clone kubespray repo "
echo -e "####################################################################\n"
git clone -b release-2.24 https://github.com/kubernetes-sigs/kubespray.git ./kubespray_ansible

echo -e "\n##################################################################"
echo -e "# Preparing kubespray inventory from template"
echo -e "####################################################################\n"
rm -rf "${KUBESPRAY}/inventory/ilyamarin-k8s"
cp -r "${KUBESPRAY}/inventory/sample" "${KUBESPRAY}/inventory/ilyamarin-k8s"

echo -e "\n##################################################################"
echo -e "# Generating hosts.yaml from terraform outputs and copy to inventory"
echo -e "####################################################################\n"
./generate_inventory.sh > "../kubespray/hosts.yaml"
cp ../kubespray/hosts.yaml "${KUBESPRAY}/inventory/ilyamarin-k8s/"

echo -e "\n##################################################################"
echo -e "# Edit kubernetes cluster name"
echo -e "####################################################################\n"
sed -i "s/cluster.local/${WS}.k8s.yc/g" "${KUBESPRAY}/inventory/ilyamarin-k8s/group_vars/k8s_cluster/k8s-cluster.yml"
cp "${KUBESPRAY}/inventory/ilyamarin-k8s/group_vars/k8s_cluster/k8s-cluster.yml" "../kubespray/k8s-cluster.yml"

echo -e "\n##################################################################"
echo -e "# Add ingress-controller"
echo -e "####################################################################\n"
sed -i "s/ingress_nginx_enabled: false/ingress_nginx_enabled: true/g" "${KUBESPRAY}/inventory/ilyamarin-k8s/group_vars/k8s_cluster/addons.yml"
sed -i "s/# ingress_nginx_host_network: false/ingress_nginx_host_network: true/g" "${KUBESPRAY}/inventory/ilyamarin-k8s/group_vars/k8s_cluster/addons.yml"
cp "${KUBESPRAY}/inventory/ilyamarin-k8s/group_vars/k8s_cluster/addons.yml" "../kubespray/addons.yml"

echo -e "\n##################################################################"
echo -e "# Creating kubernetes cluster with kubespray"
echo -e "####################################################################\n"
cd "${KUBESPRAY}"
ansible-playbook -i inventory/ilyamarin-k8s/hosts.yaml -u ilyamarin --become --become-user=root cluster.yml

echo -e "\n##################################################################"
echo -e "# Install additional stuff"
echo -e "####################################################################\n"
cd "${PWND}/terraform"
./get_k8s_config.sh