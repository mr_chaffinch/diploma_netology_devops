#!/bin/bash

set -e 

echo -e "\n##################################################################"
echo -e "# Preparing cloud infra "
echo -e "####################################################################\n"
cd terraform
terraform init -backend-config "access_key=$YC_STORAGE_ACCESS_KEY" -backend-config "secret_key=$YC_STORAGE_SECRET_KEY"
terraform workspace select prod
WS=$(terraform workspace show)
terraform apply -auto-approve