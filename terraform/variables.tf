variable "cloud_id" {
  type    = string
  default = "b1gpk77o662gkuucoc2h"
}

variable "folder_id" {
  type    = string
  default = "b1gfvk5jv38mkvmjjstv"
}

variable "dns_zone_id" {
  type    = string
  default = "dns39rum64gvc2vsfuqv"
}

variable "zones" {
  type    = list(string)
  default = ["ru-central1-a", "ru-central1-b", "ru-central1-d"]
}

variable "cidr" {
  type = map(list(string))
  default = {
    stage = ["192.168.10.0/24", "192.168.20.0/24", "192.168.30.0/24"]
    prod  = ["192.168.110.0/24", "192.168.120.0/24", "192.168.130.0/24"]
  }
}
