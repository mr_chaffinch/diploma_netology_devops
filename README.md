# Дипломная работа по курсу DevOps-инженер

Создадим руками через web-интерфейс YC s3 backet и проинициализируем terraform backend:  


![screen](./img/1.png)  
![screen](./img/2.png)  

Cоздадим базовую инфраструктуру и установим kubespray:
1) **[Скрипт генерит инвентори](/terraform/generate_inventory.sh)**
2) **[Пример инвентори](/kubespray/hosts.yaml)**
3) **[Скрипт запуска конфигурации терраформ](./setup_k8s.sh)** 
4) **[Скрипт установки доп зависимостей](./terraform/get_k8s_config.sh)**
5) **[Добавим все эти шаги в CI](https://gitlab.com/mr_chaffinch/diploma_netology_devops/-/blob/main/.gitlab-ci.yml)**

Запустим джобы в Gitlab CI: 

![screen](./img/15.png)

На скринах ниже производится настройка:
1) Инфраструктура с помощью terraform
2) Кластер кубернетис с помощью kubespray
3) Манифесты prom-stack
4) Пример запущенного приложения

![screen](./img/3.png)  
![screen](./img/3.png)  
![screen](./img/4.png)  
![screen](./img/5.png)
![screen](./img/6.png)  
![screen](./img/7.png)
![screen](./img/8.png)  
![screen](./img/9.png)
![screen](./img/10.png)  
![screen](./img/11.png)
![screen](./img/12.png)
![screen](./img/13.png)
![screen](./img/16.png)
![screen](./img/17.png)
![screen](./img/18.png)
![screen](./img/19.png)
![screen](./img/14.png)

Исправления:

1) Был добавлена джоба для применения изменения терраформ по коммиту в ветку master/main

![screen](./img/26.png)
![screen](./img/20.png)
![screen](./img/21.png)

2) Добавлевны скрины системных дэшбордов:

![screen](./img/22.png)
![screen](./img/23.png)
![screen](./img/24.png)
![screen](./img/25.png)

Примеры выполненых пайплайнов:

1) Пример успешного пайплайна для поднятия инфры **[Successful pipe infra](https://gitlab.com/mr_chaffinch/diploma_netology_devops/-/pipelines/1231865593)**
2) Пример успешного пайплайна для деплоя приложения **[Successful pipe app](https://gitlab.com/mr_chaffinch/test_app/-/pipelines/1232381110)**

### Links

1) Тестовое приложение доступно по адресу: **[http://k8s.marin-is.ru](http://k8s.marin-is.ru)**  
2) К `Grafana` можно подключиться по ссылке: **[http://k8s.marin-is.ru:8080](http://k8s.marin-is.ru:8080)**  с логином: `admin` и паролем: `Qwerty123`  
3) Репозиторий тестового приложения: **[https://gitlab.com/mr_chaffinch/test_app](https://gitlab.com/mr_chaffinch/test_app)**  
4) Пример пайплайна для закатки приложения **[https://gitlab.com/mr_chaffinch/test_app](https://gitlab.com/mr_chaffinch/test_app)** 
5) Докерфайл с тестовым приложением **[Docker file](https://gitlab.com/mr_chaffinch/test_app/-/blob/main/docker/Dockerfile?ref_type=heads)** 
6) Докерфайл для создания инфраструктуры и деплоя **[Docker file deploy](https://gitlab.com/mr_chaffinch/diploma_netology_devops/-/blob/main/Dockerfile?ref_type=heads)**
7) Репозиторий **[Terraform](https://gitlab.com/mr_chaffinch/diploma_netology_devops/-/tree/main/terraform?ref_type=heads)**  
